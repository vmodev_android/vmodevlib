/**
Leo
 */
package com.vmodev.lib.framework.localmessage;

import java.io.Serializable;

import android.os.Bundle;

public class LocalMessage {
	private final Bundle bundle = new Bundle();

	public LocalMessage() {

	}

	public void putSerializable(String key, Serializable value) {
		bundle.putSerializable(key, value);
	}

	public Serializable getSerializable(String key) {
		return bundle.getSerializable(key);
	}

	public void putString(String key, String value) {
		bundle.putString(key, value);
	}

	public String getString(String key) {
		return bundle.getString(key);
	}

	public Bundle getBundle() {
		return bundle;
	}
}
