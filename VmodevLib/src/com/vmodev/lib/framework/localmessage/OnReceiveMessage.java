/**
Leo
 */
package com.vmodev.lib.framework.localmessage;

public interface OnReceiveMessage {
	public void onReceiveMessage(String action, LocalMessage message);
}
