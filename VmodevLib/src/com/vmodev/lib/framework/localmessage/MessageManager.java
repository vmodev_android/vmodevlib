/**
Leo
 */
package com.vmodev.lib.framework.localmessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.util.Log;

public class MessageManager implements ILocalMessageService {

	private static MessageManager instant = new MessageManager();
	private MessagesService service = new MessagesService();

	private synchronized MessagesService getService() {
		return service;
	}

	public static MessageManager getManager() {
		return instant;
	}

	private MessageManager() {

	}

	@Override
	public void sendMessage(String action, LocalMessage message) {
		getService().sendMessage(action, message);
	}

	@Override
	public void register(final String action,
			final OnReceiveMessage onReceiveMessage) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				getService().register(action, onReceiveMessage);
			}
		}).start();
	}

	@Override
	public void unregister(final String action,
			final OnReceiveMessage onReceiveMessage) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				getService().unregister(action, onReceiveMessage);
			}
		}).start();
	}

	@Override
	public void cleanAll() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				getService().cleanAll();
			}
		}).start();
	}

	private class MessagesService implements ILocalMessageService {
		private final String TAG = MessagesService.class.getSimpleName();
		private Map<String, List<OnReceiveMessage>> mapMessage;

		private MessagesService() {
			mapMessage = new HashMap<String, List<OnReceiveMessage>>();
		}

		private List<OnReceiveMessage> getReceiver(String action) {
			List<OnReceiveMessage> list = null;
			if (mapMessage.containsKey(action)) {
				list = mapMessage.get(action);
			}
			return list;
		}

		@Override
		public void sendMessage(String action, LocalMessage message) {
			List<OnReceiveMessage> list = getReceiver(action);
			if (list != null) {
				for (OnReceiveMessage onReceiveMessage : list) {
					onReceiveMessage.onReceiveMessage(action, message);
				}
				Log.i(TAG, "Executed " + list.size() + " handler message");
			}
		}

		@Override
		public void register(String action, OnReceiveMessage onReceiveMessage) {
			// list of receiver
			List<OnReceiveMessage> list = getReceiver(action);
			if (list == null) {
				list = new ArrayList<OnReceiveMessage>();
				mapMessage.put(action, list);
			}
			// add onReceiveMessage to list if it has not added
			if (!list.contains(onReceiveMessage)) {
				list.add(onReceiveMessage);
				Log.i(TAG, "Register a receiver for " + action);
			}
		}

		@Override
		public void unregister(String action, OnReceiveMessage onReceiveMessage) {
			// list of receiver
			List<OnReceiveMessage> list = getReceiver(action);
			if (list != null) {
				for (Iterator iterator = list.iterator(); iterator.hasNext();) {
					OnReceiveMessage onReceive = (OnReceiveMessage) iterator
							.next();
					if (onReceive == onReceiveMessage) {
						iterator.remove();
						Log.i(TAG, "Unregister a receiver for " + action);
					}
				}
			}
		}

		@Override
		public void cleanAll() {
			mapMessage.clear();
		}
	}
}
