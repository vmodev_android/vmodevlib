/**
Leo
 */
package com.vmodev.lib.framework.localmessage;

public interface ILocalMessageService {

	public void sendMessage(String action, LocalMessage message);

	public void register(String action, OnReceiveMessage onReceiveMessage);

	public void unregister(String action, OnReceiveMessage onReceiveMessage);

	public void cleanAll();
}
