/**
Leo
 */
package com.vmodev.lib.framework.act1;

import java.util.List;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

public class BaseSyncTask extends AsyncTask<List<IAction>, Void, Void> {

	private final String TAG = BaseSyncTask.class.getSimpleName();

	public BaseSyncTask() {
	}

	@Override
	protected Void doInBackground(List<IAction>... params) {
		if (params != null && params.length > 0) {
			List<IAction> listActions = params[0];
			for (IAction action : listActions) {
				try {
					Bundle dataResult = action.onExecute();
					action.onResult(dataResult);
				} catch (Exception e) {
					Log.e(TAG, "Action fail:" + e.getMessage());
				}
			}
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
	}

}
