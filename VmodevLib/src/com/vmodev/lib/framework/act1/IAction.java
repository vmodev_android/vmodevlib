/**
Leo
 */
package com.vmodev.lib.framework.act1;

import android.os.Bundle;

public interface IAction {
	public Bundle onExecute();

	public void onResult(Bundle bundle);
}
