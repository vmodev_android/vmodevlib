/**
Leo
 */
package com.vmodev.lib.framework.act1;

import java.util.List;

public interface IEvent {
	/**
	 * This method should be only called by other thread. So we pass the
	 * IFontController as a parameter to block call it by other class
	 */
	public List<IAction> getActions(IFrontController controller);
}
