/**
Leo
 */
package com.vmodev.lib.framework.act1;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

public abstract class BaseAction implements IAction {

	private final String TAG = BaseAction.class.getSimpleName();
	private final BaseEvent event;

	public BaseAction(BaseEvent event) {
		this.event = event;
	}

	public Context getContext(){
		return event.getContext();
	}
	
	@Override
	public final Bundle onExecute() {
		Log.i(TAG, "execute " + event.getEventName());
		return handleExecute(event.getBundle());
	}

	/**
	 * You need to execute every thing over here
	 * 
	 * @param bundle
	 *            is the parameter of the Event
	 */
	protected abstract Bundle handleExecute(Bundle bundle);

	@Override
	public final void onResult(final Bundle bundle) {
		final boolean isSuccess = bundle != null;
		/**
		 * handle result on other thread
		 */
		handleResult(isSuccess, bundle);
		/**
		 * handle result on UI thread
		 */
		final OnExecuteResultOnUI onExecuteResultOnUI = event
				.getOnExecuteResuilOnUI();
		Activity parentActivity = event.getParentActivity();
		if (onExecuteResultOnUI != null && parentActivity!=null) {
			parentActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					onExecuteResultOnUI.onResultOnUI(isSuccess, bundle);
				}
			});
		}
		Log.i(TAG, "execute " + event.getEventName() + " "
				+ (isSuccess ? "success" : "fail"));
	}

	public abstract void handleResult(boolean isSuccess, Bundle ouput);
}
