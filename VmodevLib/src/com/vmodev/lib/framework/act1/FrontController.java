package com.vmodev.lib.framework.act1;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * @author Leo
 * @version 1.0
 */
public class FrontController implements IFrontController {
	private final String TAG = FrontController.class.getSimpleName();
	private static FrontController INSTANT = new FrontController();

	private FrontController() {

	}

	public static FrontController instant() {
		return INSTANT;
	}

	/**
	 * This method need to map the event with a list of actions. Then need to
	 * execute them in Asynstask. May be the mapping will expense a long time,
	 * so we need to pass the mapping action to the new service
	 * 
	 * @param event
	 * @return true if any action is executed, and else if nothing is executed
	 */
	public boolean dispatchEvent(IEvent event) {
		List<IAction> listActions = event.getActions(this);
		if (listActions != null && listActions.size() > 0) {
			Log.i(TAG, "Dispatched a event. " + listActions.size()
					+ " actions will be executed");
			// execute all of actions in AsyncTask
			BaseSyncTask task = new BaseSyncTask();
			task.execute(listActions);
			return true;
		}
		Log.i(TAG, "Dispatched a event but no action is executed");
		return false;
	}
}
