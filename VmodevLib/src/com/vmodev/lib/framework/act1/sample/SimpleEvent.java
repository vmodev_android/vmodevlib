/**
Leo
 */
package com.vmodev.lib.framework.act1.sample;

import java.util.List;

import android.content.Context;

import com.vmodev.lib.framework.act1.BaseEvent;
import com.vmodev.lib.framework.act1.IAction;

public class SimpleEvent extends BaseEvent {

	public SimpleEvent(Context context) {
		super(context);
	}

	@Override
	public void insertActions(List<IAction> list) {
		list.add(new SimpleAction(this));
	}

	@Override
	protected String getEventName() {
		return "simpleName";
	}

}
