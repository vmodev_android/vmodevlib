/**
Leo
 */
package com.vmodev.lib.framework.act1.sample;

import android.os.Bundle;

import com.vmodev.lib.framework.act1.BaseAction;
import com.vmodev.lib.framework.act1.BaseEvent;

public class SimpleAction extends BaseAction {

	public SimpleAction(BaseEvent event) {
		super(event);
	}

	/**
	 * this method use to implement the logic of the flow.
	 * 
	 * @param dataInput
	 *            is got from event
	 * @return the result data
	 */
	@Override
	protected Bundle handleExecute(Bundle dataInput) {
		return null;
	}

	@Override
	public void handleResult(boolean isSuccess, Bundle ouput) {
		
	}
}
