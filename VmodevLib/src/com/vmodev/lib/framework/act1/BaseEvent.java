/**
Leo
 */
package com.vmodev.lib.framework.act1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

/**
 * A event content all of the data which need to be perform a flow. And the
 * event need to create the instant of every actions which it want to execute
 * 
 * @author Leo
 * 
 */
public abstract class BaseEvent implements IEvent {

	private Bundle bundle;
	private final String EVENT_NAME;
	private OnExecuteResultOnUI onExecuteResuilOnUI;
	private final Context context;
	private Activity parentActivity = null;

	public BaseEvent(Context context) {
		this.context = context;
		bundle = new Bundle();
		EVENT_NAME = getEventName();
	}

	protected abstract String getEventName();

	public String getEVENT_NAME() {
		return EVENT_NAME;
	}

	public Bundle getBundle() {
		return bundle;
	}

	@Override
	public final List<IAction> getActions(IFrontController controller) {
		List<IAction> list = new ArrayList<IAction>();
		insertActions(list);
		return list;
	}

	protected abstract void insertActions(List<IAction> list);

	/**
	 * Wrapper methods
	 * 
	 * @param key
	 * @param value
	 */
	public void put(String key, String value) {
		bundle.putString(key, value);
	}

	public String getString(String key) {
		return bundle.getString(key);
	}

	public void put(String key, Serializable serializable) {
		bundle.putSerializable(key, serializable);
	}

	public Serializable getSerializable(String key) {
		return bundle.getSerializable(key);
	}

	public OnExecuteResultOnUI getOnExecuteResuilOnUI() {
		return onExecuteResuilOnUI;
	}

	public void setOnExecuteResultOnUI(Activity parentActivity,
			OnExecuteResultOnUI onExecuteResuilOnUI) {
		this.parentActivity = parentActivity;
		this.onExecuteResuilOnUI = onExecuteResuilOnUI;
	}

	public Context getContext() {
		return context;
	}

	public Activity getParentActivity() {
		return parentActivity;
	}
}
