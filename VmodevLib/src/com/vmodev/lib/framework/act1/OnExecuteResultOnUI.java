/**
Leo
 */
package com.vmodev.lib.framework.act1;

import android.os.Bundle;

public interface OnExecuteResultOnUI {
	public void onResultOnUI(boolean isSuccess, Bundle output);
}
