/**
Leo
 */
package com.vmodev.lib.common.utils;

import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TabWidget;
import android.widget.TextView;

public class ViewUtils {
	public static void setAdapter(Context context, Spinner spinner, String[] str) {
		ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(
				context, android.R.layout.simple_spinner_item, str);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
	}

	public static void setAdapter(Context context, Spinner spinner,
			List<CharSequence> str) {
		ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(
				context, android.R.layout.simple_spinner_item, str);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
	}

	// son.le
	// set enable or disable to all views in a group
	public static void setEnableViewGroup(ViewGroup viewGroup, boolean isEnabled) {
		int childCount = viewGroup.getChildCount();
		for (int i = 0; i < childCount; i++) {
			View view = viewGroup.getChildAt(i);
			view.setEnabled(isEnabled);
			if (view instanceof ViewGroup) {
				setEnableViewGroup((ViewGroup) view, isEnabled);
			}
		}
	}

	public static void showConfirmDialog(Context context, String title,
			String strBtnOk, String strBtnCancel, Integer iconId,
			DialogInterface.OnClickListener onPositiveButton,
			DialogInterface.OnClickListener onCancelButton) {
		iconId = iconId == null ? android.R.attr.alertDialogIcon : iconId;
		title = title == null ? "Are you sure?" : title;
		strBtnOk = strBtnOk == null ? "Ok" : strBtnOk;
		strBtnCancel = strBtnCancel == null ? "Cancel" : strBtnCancel;
		new AlertDialog.Builder(context).setIconAttribute(iconId)
				.setTitle(title).setPositiveButton(strBtnOk, onPositiveButton)
				.setNegativeButton(strBtnCancel, onCancelButton).create()
				.show();
	}

	public static void showConfirmDialog(Context context, String title,
			DialogInterface.OnClickListener onPositiveButton,
			DialogInterface.OnClickListener onCancelButton) {
		showConfirmDialog(context, null, null, null, null, onPositiveButton,
				onCancelButton);
	}

	public static int getImageId(Context context, String imageName) {
		return context.getResources().getIdentifier("drawable/" + imageName,
				null, context.getPackageName());
	}

	public static void setTextColorForTabs(TabWidget tabs, int color, Context context) {
		for (int index = 0; index < tabs.getChildCount(); index++) {
			TextView tvTab = (TextView) tabs.getChildAt(index).findViewById(
					android.R.id.title);
			tvTab.setTextColor(context.getResources().getColor(
					android.R.color.white));
		}
	}
}
