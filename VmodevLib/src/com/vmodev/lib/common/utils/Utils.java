/**
Leo
 */
package com.vmodev.lib.common.utils;

import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

public class Utils {
	public static void showToast(Context context, String message, int second) {
		Toast.makeText(context, message, second * 1000).show();
	}

	public static void showToast(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_LONG * 1000).show();
	}
}
